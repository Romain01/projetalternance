<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id_utilisateur',
        'nom',
        'prenom',
        'age',
        'ville',
        'num_departement',
    ];

}
