<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\Utilisateur;




class UtilisateursController extends Controller
{

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'id' => 'required|max:20',
            'mot_de_passe' => 'required|max:20',
            'confirmation_mot_de_passe' => 'required|same:mot_de_passe',
        ],
            [
            'id.required' => 'Le champ identifiant est obligatoire.',
            'id.max' => 'Le champ identifiant ne doit pas dépasser :max caractères.',
            'mot_de_passe.required' => 'Le champ mot de passe est obligatoire.',
            'confirmation_mot_de_passe.required' => 'Le champ de confirmation du mot de passe est obligatoire.',
            'confirmation_mot_de_passe.same' => 'Les deux mots de passe ne sont pas les mêmes.',
            'mot_de_passe.max' => 'Le champ mot de passe ne doit pas dépasser :max caractères.',
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if ( Utilisateur::where('identifiant', $request['id'])->first() ){
            return redirect("creerCompte")->withErrors([
                'id' => 'Cette identifiant existe déja.',
            ])->withInput();
        }

        $utilisateur = new Utilisateur();
        $utilisateur->identifiant = $request->input('id');
        $utilisateur->password = Hash::make($request->input('mot_de_passe'));
        $utilisateur->save();
        /*
        DB::table('utilisateurs')->insert([
            'identifiant' => $request->input('id'),
            'password' => Hash::make($request->input('mot_de_passe')),
        ]);
        */
        return redirect('/')->with('message', 'Votre compte a été crée, veuillez vous connecter.');;
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'mot_de_passe' => 'required',
        ],
            [
            'mot_de_passe.required' => 'Le champ mot de passe est obligatoire.',
            'id.required' => 'Le champ identifiant est obligatoire.',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = Utilisateur::where('identifiant', $request['id'])->first();

        if ($user && password_verify($request['mot_de_passe'], $user->password)) {

            session()->put('user', $user);

            return redirect('/')->with('message', 'Vous vous êtes bien connecté.');;
        }

        return redirect("connexion")->withErrors([
            'id' => 'Les informations de connexion sont invalides.',
        ])->withInput();



    }

    public function deconnexion()
    {
        session()->flush();
        return redirect('/')->with('message', 'Vous vous êtes bien déconnecté');
    }
}
