<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\Contact;

class ContactsController extends Controller
{

    public function index()
    {
        $url = 'https://geo.api.gouv.fr/departements';
        $departements = json_decode(file_get_contents($url), true);

        $contacts = Contact::where('id_utilisateur', session()->get('user')->id)->get();
        return view('index', compact('contacts', 'departements'));
    }

    public function testFormContact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nom' => 'required|alpha',
            'prenom' => 'required|alpha',
            'age' => 'required|numeric',
            'ville' => 'required',
            'num_departement' => 'required',
        ],
            [
                'nom.required' => 'Le champ du nom est obligatoire.',
                'nom.alpha' => 'Le champ du nom doit être rempli avec des lettres.',
                'prenom.required' => 'Le champ du prénom est obligatoire.',
                'prenom.alpha' => 'Le champ du prénom doit être rempli avec des lettres.',
                'age.required' => 'Le champ de de l\'age est obligatoire.',
                'age.numeric' => 'Le champ de l\'age doit être rempli avec des chiffres.',
                'ville.required' => 'Le champ de la ville est obligatoire.',
                'num_departement.required' => 'Le champ du numéro de département est obligatoire.',
                'age.numeric' => 'Le champ du numéro de département doit être rempli avec des chiffres.',
            ]);

        return ($validator);
    }

    public function create(Request $request)
    {
        $validator = $this->testFormContact($request);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if (!session()->get('user')){
            return redirect("connexion");
        }
        $contact = new Contact();
        $contact->id_utilisateur = session()->get('user')->id;
        $contact->nom = $request->input('nom');
        $contact->prenom = $request->input('prenom');
        $contact->age = $request->input('age');
        $contact->ville = $request->input('ville');
        $contact->num_departement = $request->input('num_departement');
        $contact->save();

        return redirect('/')->with('message', 'Un nouveau contact ajouté.');;
    }

    public function update(Request $request)
    {
        $validator = $this->testFormContact($request);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        if (!session()->get('user')){
            return redirect("connexion");
        }

        $contact = Contact::whereId($request->input('id'))->first();
        $contact->nom = $request->input('nom');
        $contact->prenom = $request->input('prenom');
        $contact->age = $request->input('age');
        $contact->ville = $request->input('ville');
        $contact->num_departement = $request->input('num_departement');
        $contact->save();


        return redirect('/#'.$request->input('id'))->with('message', 'Le contact '.$contact->nom." a bien été mis à jour.");
    }

    public function destroy($id)
    {
        $contact = Contact::findOrFail($id);
        $contact->delete();

        return redirect('/')->with('message', 'Contact supprimé avec succès');
    }


}
