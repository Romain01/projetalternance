<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use http\Env\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\App;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function index(){

        if ( session()->get('user') ){
            $contactsController = App::make(ContactsController::class);
            return ($contactsController->index());
        }
        return view('index');
    }

    public function connexion(){
        return view('connexion');
    }

    public function creerCompte(){
        return view('creerCompte');
    }

    public function modifierContact($id){

        $contact = Contact::findOrFail($id);

        $url = 'https://geo.api.gouv.fr/departements';
        $departements = json_decode(file_get_contents($url));

        return view('modifierContact',compact('contact','departements'));
    }

    public function departements(){

        return view('departements');
    }

}
