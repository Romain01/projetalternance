@extends('layout')

@section('content')

    <div class="container">
        <div class="card">
            <h5 class="card-header">Modifier un contact</h5>
            <form class="card-body" action="modifierContact" method="POST">
                @csrf

                <h5 class="card-title">Remplir tous les champs :</h5>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="nom">Nom</label>
                        <input type="text" pattern="[^0-9]*" class="form-control" name="nom" required title="Veuillez saisir uniquement des lettres." placeholder="Le nom" value="{{ $contact->nom }}">
                        @error('nom')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group col">
                        <label for="prenom">Prénom</label>
                        <input type="text" pattern="[^0-9]*" class="form-control" name="prenom" required title="Veuillez saisir uniquement des lettres." placeholder="Le prénom" value="{{ $contact->prenom }}">
                        @error('prenom')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group col">
                        <label for="age">Age</label>
                        <input type="text" pattern="[0-9]*" class="form-control" name="age" required title="Veuillez saisir uniquement des chiffres." placeholder="L'age" value="{{ $contact->age }}">
                        @error('age')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="ville">Ville</label>
                        <input type="text" pattern="[^0-9]*" class="form-control" name="ville" required title="Veuillez saisir uniquement des lettres." placeholder="La ville" value="{{ $contact->ville }}">
                        @error('ville')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                    <div class="form-group col-md-3">
                        <label for="num_departement">Numéro de département</label>
                        <select name="num_departement" class="custom-select">
                            @foreach ( $departements as $departement)
                                @if($departement->code == $contact->num_departement)
                                    <option selected value="{{ $departement->code }}">{{ $departement->code." ".$departement->nom }}</option>
                                @else
                                    <option value="{{ $departement->code }}">{{ $departement->code." ".$departement->nom }}</option>
                                @endif
                            @endforeach
                        </select>
                        @error('num_departement')
                        <span class="text-danger">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                <input type="hidden" name="id" value="{{ $contact->id }}">
                <button type="submit" class="btn btn-primary col">modifier</button>
            </form>
        </div>
    </div>

@endsection
