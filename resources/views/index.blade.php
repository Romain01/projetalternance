@extends('layout')

@section('content')

    <h1>Index</h1>

    @if(session()->get('user'))
        <!-- Si l'utilisateur est connecté -->

        <!-- Formulaire de création -->

        <div class="container">
            <div class="card">
                <h5 class="card-header">Ajouter un contact</h5>
                <form class="card-body" action="creerContact" method="POST">
                    @csrf

                    <h5 class="card-title">Remplir tous les champs :</h5>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="nom">Nom</label>
                            <input type="text" pattern="[^0-9]*" class="form-control" name="nom" required title="Veuillez saisir uniquement des lettres." placeholder="Le nom">
                            @error('nom')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col">
                            <label for="prenom">Prénom</label>
                            <input type="text" pattern="[^0-9]*" class="form-control" name="prenom" required title="Veuillez saisir uniquement des lettres." placeholder="Le prénom">
                            @error('prenom')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col">
                            <label for="age">Age</label>
                            <input type="text" pattern="[0-9]*" class="form-control" name="age" required title="Veuillez saisir uniquement des chiffres." placeholder="L'age">
                            @error('age')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="ville">Ville</label>
                            <input type="text" pattern="[^0-9]*" class="form-control" name="ville" required title="Veuillez saisir uniquement des lettres." placeholder="La ville">
                            @error('ville')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group col-md-3">
                            <label for="num_departement">Numéro de département</label>
                            <select name="num_departement" class="custom-select">
                                <option selected value="">...</option>
                                @foreach ( $departements as $departement)
                                    <option value="{{ $departement['code'] }}">{{ $departement['code']." ".$departement['nom'] }}</option>
                                @endforeach
                            </select>
                            @error('num_departement')
                            <span class="text-danger">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary col">ajouter</button>
                </form>
            </div>
        </div>

        <!-- Liste des contacts -->

        @if (count($contacts)!=0)
            <div class="container">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">id</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Prénom</th>
                        <th scope="col">Age</th>
                        <th scope="col">Ville</th>
                        <th scope="col">Numéro département</th>
                        <th scope="col">Date ajout</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <!-- Pour chaque contact on ajoute une ligne -->
                    @foreach ($contacts as $contact)
                        <tr>
                            <form action="modifierContact/{{ $contact->id }}" method="GET">
                                <!-- Fotmulaire pour modifier -->
                                @csrf
                                <td>
                                    <!-- Ancre pour revenir sur cette ligne après modification -->
                                    <a id="{{ $contact->id }}">{{ $contact->id }}</a>
                                </td>
                                <td>{{ $contact->nom }}</td>
                                <td>{{ $contact->prenom }}</td>
                                <td>{{ $contact->age }}</td>
                                <td>{{ $contact->ville }}</td>
                                <td>{{ $contact->num_departement }}</td>
                                <td>{{ $contact->created_at }}</td>
                                <td><button class="btn col">Modifier</button></td>
                            </form>
                            <form action="supprimerContact/{{ $contact->id }}" method="POST">
                                <!-- Pour supprimer ce contact -->
                                @csrf
                                @method('DELETE')
                                <td><button class="btn col">Supprimer</button></td>
                            </form>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif

    @else
        <!-- Si l'utilisateur n'est pas connecté -->
        <p>Veuillez vous <a href="connexion">connecter</a> ou <a href="creerCompte">créer</a> un compte.</p>
    @endif
@endsection
