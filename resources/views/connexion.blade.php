@extends('layout')

@section('content')

    <div class="container">
        <div class="card">
            <h1 class="card-header">Connecter vous.</h1>

            <form class="card-body" method="POST" action="connexion">

                @csrf
                <p>
                    <label for="textInput">Votre identifiant :</label>
                    <input name="id" type="text" placeholder="identifiant" value="{{ old('id') }}">
                    @error('id')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </p>

                <p>
                    <label for="textInput">Votre mot de passe :</label>
                    <input name="mot_de_passe" type="password" placeholder="mot de passe">
                    @error('mot_de_passe')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                </p>

                <button type="submit">Se connecter</button>

            </form>
        </div>
    </div>

@endsection
