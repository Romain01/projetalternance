import './bootstrap';

import {createApp} from 'vue/dist/vue.esm-bundler.js';
import Departements from './components/departementComponent.vue';

const app = createApp({});

app.component('departement-component', Departements);

app.mount("#app");
