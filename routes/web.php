<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be     assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [\App\Http\Controllers\Controller::class, 'index']);

Route::get('connexion', [\App\Http\Controllers\Controller::class, 'connexion']);

Route::get('creerCompte', [\App\Http\Controllers\Controller::class, 'creerCompte']);

Route::get('deconnexion', [\App\Http\Controllers\UtilisateursController::class, 'deconnexion']);

Route::get('modifierContact/{id}', [\App\Http\Controllers\Controller::class, 'modifierContact']);

Route::get('departements', [\App\Http\Controllers\UtilisateursController::class, 'departements']);

Route::post('creerCompte', [\App\Http\Controllers\UtilisateursController::class, 'create']);

Route::post('connexion', [\App\Http\Controllers\UtilisateursController::class, 'login']);

Route::post('creerContact', [\App\Http\Controllers\ContactsController::class, 'create']);

Route::post('modifierContact/{id}', [\App\Http\Controllers\ContactsController::class, 'update']);

Route::delete('supprimerContact/{id}', [\App\Http\Controllers\ContactsController::class, 'destroy']);
